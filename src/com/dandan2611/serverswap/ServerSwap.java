package com.dandan2611.serverswap;

import com.dandan2611.serverswap.commands.ShowRedirectedPlayersCommand;
import com.dandan2611.serverswap.listeners.PlayerJoinListener;
import com.dandan2611.serverswap.utils.ServerRediriger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class ServerSwap extends JavaPlugin {

    public ServerRediriger serverRediriger;

    public List<Player> redirectedPlayers;

    @Override
    public void onLoad() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "[ServerRediriger] Loading and enabling plugin...");
        this.serverRediriger = new ServerRediriger(this);
        this.redirectedPlayers = new ArrayList<>();
    }

    @Override
    public void onEnable() {
        registerListeners();
        registerCommands();
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ServerRediriger] Plugin ready !");
    }

    private void registerListeners() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new PlayerJoinListener(this), this);
    }

    private void registerCommands() {
        Bukkit.getPluginCommand("showplayers").setExecutor(new ShowRedirectedPlayersCommand(this));
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "[ServerRediriger] Disabling plugin...");
        serverRediriger = null;
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[ServerRediriger] Plugin disabled !");
    }

}
