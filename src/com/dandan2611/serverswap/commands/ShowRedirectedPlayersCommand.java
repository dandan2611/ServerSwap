package com.dandan2611.serverswap.commands;

import com.dandan2611.serverswap.ServerSwap;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ShowRedirectedPlayersCommand implements CommandExecutor {

    private ServerSwap PLUGIN;

    public ShowRedirectedPlayersCommand(ServerSwap plugin) {
        this.PLUGIN = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(!p.isOp()) {
                p.sendMessage(ChatColor.RED + "Vous devez être opérateur pour pouvoir exécuter cette commande !");
                return true;
            }
            p.sendMessage(ChatColor.GRAY + "Joueurs ayant été redirigés :");
            for(Player player : PLUGIN.redirectedPlayers) {
                String playerName = player.getName();
                stringBuilder.append(playerName + " ; ");
            }
            p.sendMessage(ChatColor.GOLD + stringBuilder.toString());
            return true;
        }
        else if(sender instanceof ConsoleCommandSender) {
            System.out.println("Joueurs ayant été rédirigés :");
            for(Player player : PLUGIN.redirectedPlayers) {
                String playerName = player.getName();
                stringBuilder.append(playerName + " ; ");
            }
            System.out.println(stringBuilder.toString());
            return true;
        }
        return true;
    }
}
