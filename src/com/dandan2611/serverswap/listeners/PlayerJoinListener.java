package com.dandan2611.serverswap.listeners;

import com.dandan2611.serverswap.ServerSwap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;

public class PlayerJoinListener implements Listener {

    private ServerSwap serverSwap;

    public PlayerJoinListener(ServerSwap serverSwap) {
        this.serverSwap = serverSwap;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        Inventory inventory = p.getInventory();
        inventory.clear();

        p.sendMessage(ChatColor.GRAY + "Votre inventaire à été nettoyé avec " + ChatColor.GREEN + "succès " + ChatColor.GRAY + "!");

        serverSwap.serverRediriger.redirectLater(p, "bonjour", 3, true);
    }

}
