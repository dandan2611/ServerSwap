package com.dandan2611.serverswap.utils;

import com.dandan2611.serverswap.ServerSwap;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ServerRediriger {

    private ServerSwap PLUGIN;

    public ServerRediriger(ServerSwap plugin) {
        this.PLUGIN = plugin;
    }

    public void redirectPlayer(Player player, String serverName) {
        // Création du serializer
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        // Remplissage
        out.writeUTF("connect");
        out.writeUTF(serverName);
        // Envoi du plugin message via le joueur ciblé
        player.sendPluginMessage(PLUGIN, "BungeeCord", out.toByteArray());
        PLUGIN.redirectedPlayers.add(player);
    }

    public void redirectLater(Player player, String serverName, int seconds, boolean showMesage) {
        int secondes = seconds * 10;
        if(showMesage)
            player.sendMessage(ChatColor.GRAY + "Redirection vers le serveur " + ChatColor.GOLD + serverName + ChatColor.GRAY + " dans " + ChatColor.GREEN + secondes + " seconde(s)" + ChatColor.GRAY + "...");
        Bukkit.getScheduler().runTaskLater(PLUGIN,
                () -> {

                    redirectPlayer(player, serverName);
                    PLUGIN.redirectedPlayers.add(player);

                }
                , secondes);
    }

}
